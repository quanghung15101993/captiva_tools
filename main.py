from kivymd.app import MDApp
from kivy.core.window import Window
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen
from home.applications.menu import MenuApplication

Window.size = (1000, 800)

class MenuLogin(Screen):
    pass

class MenuApplication(Screen):
    pass


class MainApp(MDApp):
    def build(self):
        self.theme_cls.primary_palette = "Red"
        self.sm = ScreenManager()
        self.sm.add_widget(MenuLogin())
        self.sm.add_widget(MenuApplication())
        return self.sm

    def verify(self, email, password):
        if email == "hunglq.os" and password == "123":
            print("done")


if __name__ == '__main__':
    MainApp().run()
