from kivy.lang import Builder
from kivy.uix.screenmanager import Screen
from kivy.uix.boxlayout import BoxLayout
from kivymd.theming import ThemableBehavior
from kivymd.uix.list import MDList

Builder.load_string("""
<MenuApplication>:
    name: 'application'
    Screen:
    NavigationLayout:
        ScreenManager:
            Screen:
                BoxLayout:
                    orientation: 'vertical'
                    MDToolbar:
                        title: 'Application'
                        left_action_items: [["menu", lambda x: nav_drawer.set_state('toggle')]]
                        elevation:5
                    Widget:
        MDNavigationDrawer:
            id: nav_drawer
            ContentNavigationDrawer:
                orientation: 'vertical'
                padding: "8dp"
                spacing: "8dp"
                ScrollView:
                    DrawerList:
                        id: md_list        
                        MDList:
                            OneLineIconListItem:
                                text: "ImageProcessing"  
                                on_release: print("Click!")                         
                                IconLeftWidget:
                                    icon: "image"
                            OneLineIconListItem:
                                text: "Logout"  
                                on_release: root.manager.current = 'login'                         
                                IconLeftWidget:
                                    icon: "logout"
""")


class MenuApplication(Screen):
    pass


class NavigationLayout(Screen):
    pass


class ContentNavigationDrawer(BoxLayout):
    pass


class DrawerList(ThemableBehavior, MDList):
    def on_release(self):
        print(self)


class NavigationDrawer(Screen):
    pass
